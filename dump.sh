#!/bin/bash 

default_size=100
sys_call_table_sz=1348


get_sym() {
	sym=$1
	typ=$2
	addr="0x$(grep $sym /proc/kallsyms  | grep $typ | head -1 | cut -d ' ' -f 1)"
	echo $addr 
}

check_sym() {
	sym=$1
	typ=$2
	sz=$3
	addr=$(get_sym $sym $typ)
	echo "0x$addr $sz" > /proc/khash
	sha1=$(cat /proc/khash)
	echo "$sha1 $sym"
}

check_modules() {
	echo "si"
}



main() {
	insmod khash.ko 2>/dev/null

	sha1sum /proc/modules 
	sha1sum /proc/kallsyms
	sha1sum /boot/initrd.img-`uname -r`
	sha1sum /boot/vmlinuz-`uname -r`

	check_sym sys_call_table R $sys_call_table_sz

	#check_sym sys_call_table T $sys_call_table_sz

	echo "-- functions and tables --"

	for sym in `cat /proc/kallsyms | sed 's/ /,/g'`
	do
		addr=$(echo $sym | cut -d ',' -f 1)
		typ=$(echo $sym | cut -d ',' -f 2)
		name=$(echo $sym | cut -d ',' -f 3)

		if [ $typ == "T" -o $typ == "R" ]; then
			echo "0x$addr 100" > /proc/khash
			sha1=$(cat /proc/khash)
			echo "$sha1 $name"
		fi
	done
}



main



extern char *hash_sha1(const unsigned char *data, unsigned int len);
extern char *hash_to_string(const unsigned char *hash);
extern void hash_print(const char *tag, const unsigned char *hash);

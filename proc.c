/*
	proc communication
	sha0@badchecksum.net

*/


#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/syscalls.h>
#include <asm/uaccess.h>
#include <linux/cdev.h>
#include <linux/proc_fs.h>

#include "macros.h"
#include "check.h"
#include "hash.h"

#define MAX_OUTPUT 0xfff
#define MAX_IN 0xff
#define PROC_ENTRY "khash"


int pos;
char out[MAX_OUTPUT];
char in[MAX_IN];


static int proc_read(char *page, char **start, off_t off, int count, int *eof, void *data);
static ssize_t proc_write(struct file *filp, const char __user * buff, unsigned long len, void *data);


void proc_print(const char *msg) {
	int sz = strlen(msg);

	if (sz+pos >= MAX_OUTPUT)
		sz = MAX_OUTPUT-pos-2;

	memcpy(&out[pos],msg,sz);
	pos += sz;
}

void proc_unregister(void) {
    remove_proc_entry(PROC_ENTRY, NULL);
}

void proc_clean(void) {
	pos = 0;
 	memset(out, 0x00, MAX_OUTPUT);
}
 
int proc_register(void) {
    struct proc_dir_entry *proc;

 	proc_clean();
    proc = create_proc_entry(PROC_ENTRY, S_IFREG, NULL);
    if (!proc) {
    	LOG("ALERT! cann't create /proc/khash");
        return -1;
    }
    proc->read_proc = proc_read;
    proc->write_proc = proc_write;
	proc->mode = S_IFREG | S_IRUGO;
	proc->uid = 0;
	proc->gid = 0;
	proc->size = 37;

    return 0;
}


static int proc_read(char *page, char **start, off_t off, int count, int *eof, void *data) {
	int len = sprintf(page,"%s",out);
	return len;
}


static ssize_t proc_write(struct file *filp, const char __user * buff, unsigned long len, void *data) {
	int sz = 0;
	unsigned long l;

	proc_clean();

	if (len < 12)
		return -EFAULT;

	if (copy_from_user(in, buff, MAX_IN)) {
		return -EFAULT;
	}

	if (len > MAX_IN)
		len = MAX_IN;
	
	in[MAX_IN-1] = 0x00;

	sscanf(in,"0x%lx %d", &l, &sz);
	check_run(l,sz);

	return len;
}


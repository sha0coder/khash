// IDT
struct {
        unsigned short limit;
        unsigned int base;
} __attribute__ ((packed)) idtr;
 
struct {
        unsigned short off1;
        unsigned short sel;
        unsigned char none,flags;
        unsigned short off2;
} __attribute__ ((packed)) idt_entry;
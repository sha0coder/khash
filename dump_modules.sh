 #!/bin/bash


for lkm in `cat /proc/modules | awk '{ print $1","$2","$6 }'`
do
		name=$(echo $lkm | cut -d ',' -f 1)
		sz=$(echo $lkm | cut -d ',' -f 2)
		addr=$(echo $lkm | cut -d ',' -f 3)
		echo "$addr $sz" > /proc/khash
		sha1=$(cat /proc/khash)
		echo "$sha1 $name"
done

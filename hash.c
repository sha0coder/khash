#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/crypto.h>
#include <linux/scatterlist.h>
#include "macros.h"

#define MAX_DROWDS 255
#define STRHASH_LEN 21*2+2


void hash_print(const char *tag, const unsigned char *hash) { //TODO: use hash_to_string() instead of repeat code
	char out[50];
	int i;
	
	if (hash == NULL) {
		printk(KERN_CRIT "%s: - \n",tag);
		return;
	}

	for (i=0; i<21; i++)  
		sprintf(out+i*2,"%.2x",hash[i]);

	printk(KERN_CRIT "%s: 0x%s",tag,out);
}

char *hash_to_string(const unsigned char *hash) {
	char *out;
	int i;

	out = (char *)kmalloc(STRHASH_LEN, GFP_KERNEL);
	memset(out,0x00,STRHASH_LEN);

	if (hash == NULL)
		return NULL;

	for (i=0; i<20; i++)  
		sprintf(out+i*2,"%.2x",hash[i]);

	return out;
}



char *hash_sha1(const unsigned char *data, unsigned int sz) {
	int ret;
	char *output_buf;
	struct scatterlist sg[1];
	struct hash_desc desc;
	struct crypto_hash *tfm = crypto_alloc_hash("sha1", 0, CRYPTO_ALG_ASYNC);
	if (tfm == NULL) {
		LOG("crypto_alloc failed.");
	    return NULL;
	}
	
	output_buf = kmalloc(crypto_hash_digestsize(tfm), GFP_KERNEL);
	if (output_buf == NULL) {
		LOG("kmalloc failed.");
	    return NULL;
	}

	desc.tfm = tfm;
	ret = crypto_hash_init(&desc);
	if (ret != 0) {
		LOG("crypto_hash_init failed.");
	    return NULL;
	}
	sg_init_table(sg, ARRAY_SIZE(sg));
	sg_set_buf(&sg[0], data, sz);
	ret = crypto_hash_digest(&desc, sg, sz, output_buf);
	if (ret != 0) {
		LOG("crypto_hash_digest failed.");
	    return NULL;
	}

	return output_buf;
}  //from http://stackoverflow.com/questions/3869028/how-to-use-cryptoapi-in-the-linux-kernel-2-6

extern unsigned long **st;  //sys_call_table;
extern unsigned long **sys_call_table;

extern char syscall_table_lookup_3_0(void);
extern char syscall_table_lookup_2_6(void);
extern void disable_page_protection(void);
extern void enable_page_protection(void);

#define NUM_SYSCALLS 338



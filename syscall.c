/*
        Kernel util algorithms v0.1
        sha0@badchecksum.net
*/



#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/syscalls.h>
#include <asm/unistd.h>
#include "idt.h"


unsigned long **st;  //sys_call_table;
unsigned long **sys_call_table;




char syscall_table_lookup_3_0(void) {
        unsigned long int offset = PAGE_OFFSET;

        while (offset < ULLONG_MAX) {
                st = (unsigned long **)offset;

                if (st[__NR_close] == (unsigned long *) sys_close)
                        return 1;

                offset += sizeof(void *);
        }

        return 0;
}

char syscall_table_lookup_2_6(void)  {
        char *sys_call_off;
        char *p;


        asm ("sidt %0" : "=m" (idtr));
        memcpy (&idt_entry, (char *)(idtr.base+8*0x80), sizeof(idt_entry));
        sys_call_off = (char *)((idt_entry.off2 << 16) | idt_entry.off1);
 
        for (p = sys_call_off; p < sys_call_off+500; p++) {
                if (p[0] == (char)0xff && p[1] == (char)0x14 && p[2] == (char)0x85) {
                        st = (unsigned long **)(*(unsigned long *)(p+3));
			//printk("syscall table found!!!\n");
			return 1;
                }
        }
	printk("syscall table not found :/\n");
	return 0;
}

void disable_page_protection(void) {
        unsigned long value;
        asm volatile("mov %%cr0, %0" : "=r" (value));

        if(!(value & 0x00010000))
                return;

        asm volatile("mov %0, %%cr0" : : "r" (value & ~0x00010000));
}

void enable_page_protection(void) {
        unsigned long value;
        asm volatile("mov %%cr0, %0" : "=r" (value));

        if((value & 0x00010000))
                return;

        asm volatile("mov %0, %%cr0" : : "r" (value | 0x00010000));
}

void log(const char *msg) {
        printk(KERN_CRIT "%s\n",msg);
}

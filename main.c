/*
	khash
	Kernel integrity check
	v0.2
	sha0@badchecksum.net
*/


#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>

#include <asm/unistd.h>
#include <linux/version.h>
#include <asm/fcntl.h>
#include <linux/types.h>

#include "macros.h"
#include "proc.h"
#include "hash.h"


int init_module(void) {
	proc_register();

    return 0;
}

void cleanup_module(void) {
	proc_unregister();
}


MODULE_LICENSE ("GPL");
MODULE_AUTHOR ("sha0@badchecksum.net");
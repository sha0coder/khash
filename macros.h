#define APP "khash"
#define LOG(m) printk(KERN_CRIT APP ": %s\n", m)
#define LOGN(n) printk(KERN_CRIT APP ": %d\n", n)
#define FIT(n,b,t) n = (n<b?b:n); n = (n>t?t:n)


#define ISSPACE(c)  ((c) == ' ' || ((c) >= '\t' && (c) <= '\r'))
#define ISASCII(c)  (((c) & ~0x7f) == 0)
#define ISUPPER(c)  ((c) >= 'A' && (c) <= 'Z')
#define ISLOWER(c)  ((c) >= 'a' && (c) <= 'z')
#define ISALPHA(c)  (ISUPPER(c) || ISLOWER(c))
#define ISDIGIT(c)  ((c) >= '0' && (c) <= '9')
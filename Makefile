obj-m := khash.o 
khash-objs := proc.o hash.o check.o main.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules -Wunused-function


clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

	
